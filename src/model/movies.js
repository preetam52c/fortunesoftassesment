const { Number, Date } = require('mongoose');
const mongoose = require('mongoose');
const { Schema } = mongoose;

const MoviesSchema = new Schema({
    backdrop: {
        type: String
    },
    cast: {
        type: [{type: String}]
    },
    classification: {
        type: String
    },
    director: {
        type: [{type: String}]
    }, 
    genres: {
        type: [{type: String}]
    },
    id: {
        type: String
    },
    imdb_rating: {
        type: Number
    },
    length: {
        type: String
    }, 
    overview: {
        type: String
    }, 
    poster: {
        type: String
    }, 
    released_on: {
        type: Date
    }, 
    slug: {
        type: String
    }, 
    title: {
        type: String
    }
})

const moviesModel = mongoose.model('movies', MoviesSchema);


module.exports = {
    moviesModel
}