const verifyToken = (req, res, next) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        const token = req.headers.authorization.split(' ')[1];
        if(token === process.env.token) {
            next();
        }
        else {
            res.status(403).send({error: "Token is invalid, please provide a valid token"})
        }
    } else {
        res.status(403).send({error: "Token is invalid, please provide a valid token"})
    }
}

module.exports = {
    verifyToken
};