const moviesService = require('../model/movies');

const groupFields = ['backdrop', 'cast', 'classification', 'director', 'id', 'imdb_rating', 'length', 'overview', 'poster', 'released_on', 'slug', 'title']

const groupMapping = () => {
    const obj = {};
    groupFields.forEach((key) => {
        obj[key] = '$'+key
    })
    return obj
}
const getMoviesByGeneres = async(req, res) => {

    try {
        let movies = await moviesService.moviesModel.aggregate([{$unwind: '$genres'}, {$group: {
            _id: '$genres',
            movies: {$push: groupMapping()}
        }}]);
        movies = movies.map((e) => {
            e.genres = e._id;
            delete e['_id'];
            return e
        })
        res.status(200).send({data: movies})

    } catch (error) {
        console.log("error",error);
        res.status(400).send({error: `${error}`})
    }

}

module.exports = {
    getMoviesByGeneres
}