const express = require('express');
const router = express.Router();
const moviesController = require('../controller/movies');
const { verifyToken } = require('../services/tokenVerification');

router.get('/getMoviesByGenres', verifyToken, moviesController.getMoviesByGeneres);

module.exports = router;