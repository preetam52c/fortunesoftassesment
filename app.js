const express = require('express');
var cors = require('cors')
const app = express()
const dotenv = require('dotenv')
dotenv.config()
const connectDB = require('./src/config/dbConnection')
const movieRoutes = require('./src/routes/movies')

app.use(cors())
connectDB()

app.options('*', cors());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", '*'); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}))

app.use('/api', movieRoutes)

app.get('/', (req, res) => {
    res.send({ message: "server is running" })
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log('server is running'))
